#![feature(core_intrinsics, trait_alias)]

mod uid;

// Reexports
pub use sum_type::sum_type;
pub use uid::Uid;

use std::{
    intrinsics::type_id,
    marker::PhantomData,
    collections::{HashMap, HashSet},
    fmt::Debug,
    convert::{TryFrom, TryInto},
};
use specs::{
    self,
    Join,
    Builder,
    saveload::MarkerAllocator,
};
#[cfg(feature = "serde1")]
use serde::{Serialize, Deserialize, de::DeserializeOwned};
//#[cfg(feature = "serde1")]
//use serde_derive::{Serialize, Deserialize};

#[cfg(feature = "serde1")]
pub trait SerdeAlias = Serialize + DeserializeOwned;

#[cfg(not(feature = "serde1"))]
pub trait SerdeAlias = ;

pub trait Packet: Clone + Debug + sum_type::SumType + Send + 'static {
    type Phantom: Clone + Debug + SerdeAlias;
}

#[derive(Copy, Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub enum UpdateKind<P: Packet> {
    Inserted(P),
    Modified(P),
    Removed(P::Phantom),
}

#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct StatePackage<P: Packet> {
    spectrum: u16,
    entities: HashMap<u64, Vec<P>>,
}

impl<P: Packet> StatePackage<P> {
    pub fn derive_filtered<F: FnMut(u64, &P) -> bool>(&self, mut f: F) -> Self {
        Self {
            spectrum: self.spectrum,
            entities: self.entities
                .iter()
                .map(|(uid, p)| (*uid, p.iter().filter(|p| f(*uid, p)).cloned().collect()))
                .collect(),
        }
    }
}

impl<P: Packet> Default for StatePackage<P> {
    fn default() -> Self {
        Self {
            spectrum: 0,
            entities: HashMap::new(),
        }
    }
}

#[derive(Clone, Debug)]
#[cfg_attr(feature = "serde1", derive(Serialize, Deserialize))]
pub struct SyncPackage<P: Packet> {
    comp_updates: Vec<(u64, UpdateKind<P>)>,
    created_entities: HashSet<u64>,
    deleted_entities: HashSet<u64>,
}

impl<P: Packet> SyncPackage<P> {
    pub fn derive_filtered<F: FnMut(u64, &UpdateKind<P>) -> bool>(&self, mut f: F) -> Self {
        Self {
            comp_updates: self.comp_updates.iter().filter(|(uid, p)| f(*uid, p)).cloned().collect(),
            created_entities: self.created_entities.clone(),
            deleted_entities: self.deleted_entities.clone(),
        }
    }
}

pub struct World<P: Packet> {
    specs_world: specs::World,
    trackers: HashMap<u64, Box<dyn Tracker<P>>>,
    created_entities: HashSet<u64>,
    deleted_entities: HashSet<u64>,
}

impl<P: Packet> std::ops::Deref for World<P> {
    type Target = specs::World;
    fn deref(&self) -> &specs::World {
        &self.specs_world
    }
}

impl<P: Packet> std::ops::DerefMut for World<P> {
    fn deref_mut(&mut self) -> &mut specs::World {
        &mut self.specs_world
    }
}

impl<P: Packet> World<P> {
    #[allow(dead_code)]
    pub fn new<F: FnOnce(&mut Self)>(specs_world: specs::World, register_fn: F) -> Self {
        Self::from_state_package(specs_world, register_fn, StatePackage::default())
    }

    #[allow(dead_code)]
    pub fn from_state_package<F: FnOnce(&mut Self)>(
        mut specs_world: specs::World,
        register_fn: F,
        state_package: StatePackage<P>,
    ) -> Self {
        specs_world.register::<uid::Uid>();
        specs_world.add_resource(uid::UidAllocator::new(state_package.spectrum));

        let mut this = Self {
            specs_world,
            trackers: HashMap::new(),
            created_entities: HashSet::new(),
            deleted_entities: HashSet::new(),
        };

        // Call a function to register types
        register_fn(&mut this);

        // Apply state package entities
        for (entity_uid, packets) in state_package.entities {
            let entity = this.create_entity_with_uid(entity_uid);
            for packet in packets {
                this.trackers
                    .values()
                    .for_each(|tracker| tracker.try_insert_packet(&this.specs_world, entity, packet.clone()));
            }
        }

        this.specs_world.maintain();

        // Clear change trackers to avoid echoing changes
        this.clear_tracker_events();
        this
    }

    fn clear_tracker_events(&mut self) {
        self.created_entities.clear();
        self.deleted_entities.clear();
        for tracker in self.trackers.values_mut() {
            tracker.clear_events(&self.specs_world);
        }
    }

    #[allow(dead_code)]
    pub fn register_synced<C: specs::Component + Clone + Send + Sync>(&mut self)
        where
            P: From<C>, C: TryFrom<P, Error=sum_type::InvalidType>,
            P::Phantom: From<PhantomData<C>>, P::Phantom: TryInto<PhantomData<C>, Error=sum_type::InvalidType>,
            C::Storage: Default + specs::storage::Tracked,
    {
        self.specs_world.register::<C>();

        self.trackers.insert(
            unsafe { type_id::<C>() },
            Box::new(UpdateTracker::<C> {
                reader_id: self.specs_world
                    .write_storage::<C>()
                    .register_reader(),
                inserted: specs::BitSet::new(),
                modified: specs::BitSet::new(),
                removed: specs::BitSet::new(),
                phantom: PhantomData,
            }),
        );
    }

    #[allow(dead_code)]
    pub fn create_entity_synced(&mut self) -> specs::EntityBuilder {
        let entity_builder = self.specs_world.create_entity();
        let uid = entity_builder.world
            .write_resource::<uid::UidAllocator>()
            .allocate(entity_builder.entity, None);
        self.created_entities.insert(uid.into());
        entity_builder
            .with(uid)
    }

    #[allow(dead_code)]
    pub fn delete_entity_synced(&mut self, entity: specs::Entity) -> Result<(), specs::error::WrongGeneration> {
        let uid = self.specs_world
            .read_storage::<uid::Uid>()
            .get(entity)
            .cloned();

        if let Some(uid) = uid {
            if !self.created_entities.remove(&uid.into()) {
                self.deleted_entities.insert(uid.into());
            }
            self.specs_world.delete_entity(entity)
        } else {
            Ok(())
        }
    }

    /// Get the UID of an entity
    pub fn uid_from_entity(&self, entity: specs::Entity) -> Option<u64> {
        self.specs_world
            .read_storage::<uid::Uid>()
            .get(entity)
            .cloned()
            .map(|uid| uid.into())
    }

    /// Get the UID of an entity
    pub fn entity_from_uid(&self, uid: u64) -> Option<specs::Entity> {
        self.specs_world
            .read_resource::<uid::UidAllocator>()
            .retrieve_entity_internal(uid)
    }

    #[allow(dead_code)]
    pub fn gen_state_package(&self) -> StatePackage<P> {
        StatePackage {
            spectrum: self.specs_world.write_resource::<uid::UidAllocator>().next_spectrum(),
            entities: (
                    &self.specs_world.entities(),
                    &self.specs_world.read_storage::<uid::Uid>(),
                )
                .join()
                .map(|(entity, &entity_uid)| {
                    let mut packets = Vec::new();
                    for tracker in self.trackers.values() {
                        tracker.add_packet_for(&self.specs_world, entity, &mut packets);
                    }
                    (entity_uid.into(), packets)
                })
                .collect(),
        }
    }

    fn create_entity_with_uid(&mut self, entity_uid: u64) -> specs::Entity {
        let existing_entity = self.specs_world
            .read_resource::<uid::UidAllocator>()
            .retrieve_entity_internal(entity_uid.into());

        match existing_entity {
            Some(entity) => entity,
            None => {
                let entity_builder = self.specs_world
                    .create_entity();
                let uid = entity_builder.world
                    .write_resource::<uid::UidAllocator>()
                    .allocate(entity_builder.entity, Some(entity_uid));
                entity_builder
                    .with(uid)
                    .build()
            },
        }
    }

    #[allow(dead_code)]
    pub fn sync_with_package(&mut self, package: SyncPackage<P>) {
        // Attempt to create entities
        for entity_uid in package.created_entities.into_iter() {
            self.create_entity_with_uid(entity_uid);
        }

        for (entity_uid, update) in package.comp_updates
            .into_iter()
        {
            if let Some(entity) = self.specs_world
                .read_resource::<uid::UidAllocator>()
                .retrieve_entity_internal(entity_uid)
            {
                match update {
                    UpdateKind::Inserted(packet) => {
                        self.trackers
                            .values()
                            .for_each(|tracker| tracker.try_insert_packet(&self.specs_world, entity, packet.clone()));
                    },
                    UpdateKind::Modified(packet) => {
                        self.trackers
                            .values()
                            .for_each(|tracker| tracker.try_modify_packet(&self.specs_world, entity, packet.clone()));
                    },
                    UpdateKind::Removed(phantom) => {
                        self.trackers
                            .values()
                            .for_each(|tracker| tracker.try_remove_packet(&self.specs_world, entity, phantom.clone()));
                    },
                }
            }
        }

        // Attempt to delete entities that were marked for deletion
        for entity_uid in package.deleted_entities.into_iter() {
            let entity = self.specs_world
                .read_resource::<uid::UidAllocator>()
                .retrieve_entity_internal(entity_uid);
            if let Some(entity) = entity {
                let _ = self.specs_world.delete_entity(entity);
            }
        }

        // Clear tracker events to avoid an echo chamber
        self.clear_tracker_events();
    }

    #[allow(dead_code)]
    pub fn next_sync_package(&mut self) -> SyncPackage<P> {
        // Collect changed entity updates
        let mut comp_updates = Vec::new();
        for tracker in self.trackers.values_mut() {
            tracker.get_updates_of(&self.specs_world, &mut comp_updates);
        }

        // Swap out created / deleted entities
        let (mut created_entities, mut deleted_entities) = (HashSet::new(), HashSet::new());
        std::mem::swap(&mut created_entities, &mut self.created_entities);
        std::mem::swap(&mut deleted_entities, &mut self.deleted_entities);

        SyncPackage {
            comp_updates,
            created_entities,
            deleted_entities,
        }
    }
}

pub trait Tracker<P: Packet>: Send + 'static {
    fn add_packet_for(&self, specs_world: &specs::World, entity: specs::Entity, packets: &mut Vec<P>);
    fn try_insert_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P);
    fn try_modify_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P);
    fn try_remove_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P::Phantom);
    fn clear_events(&mut self, specs_world: &specs::World);
    fn get_updates_of(&mut self, world: &specs::World, out: &mut Vec<(u64, UpdateKind<P>)>);
}

struct UpdateTracker<C: specs::Component> {
    reader_id: specs::ReaderId<specs::storage::ComponentEvent>,
    inserted: specs::BitSet,
    modified: specs::BitSet,
    removed: specs::BitSet,
    phantom: PhantomData<C>,
}

impl<C: specs::Component + Clone + Send + Sync, P: Packet> Tracker<P> for UpdateTracker<C>
    where
        P: From<C>, C: TryFrom<P, Error=sum_type::InvalidType>,
        P::Phantom: From<PhantomData<C>>, P::Phantom: TryInto<PhantomData<C>, Error=sum_type::InvalidType>,
        C::Storage: specs::storage::Tracked,
{
    fn add_packet_for(&self, specs_world: &specs::World, entity: specs::Entity, packets: &mut Vec<P>) {
        if let Some(comp) = specs_world.read_storage::<C>().get(entity) {
            packets.push(P::from(comp.clone()));
        }
    }

    fn try_insert_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P) {
        if let Ok(comp) = packet.try_into() {
            let _ = specs_world.write_storage::<C>().insert(entity, comp);
        }
    }

    fn try_modify_packet(&self, specs_world: &specs::World, entity: specs::Entity, packet: P) {
        if let Ok(comp) = packet.try_into() {
            let _ = specs_world.write_storage::<C>().get_mut(entity).map(|c| *c = comp);
        }
    }

    fn try_remove_packet(&self, specs_world: &specs::World, entity: specs::Entity, phantom: P::Phantom) {
        if let Ok(_) = phantom.try_into() {
            let _ = specs_world.write_storage::<C>().remove(entity);
        }
    }

    fn clear_events(&mut self, specs_world: &specs::World) {
        // Consume all events
        specs_world.write_storage::<C>()
            .channel()
            .read(&mut self.reader_id)
            .for_each(|_| {});
    }

    fn get_updates_of(&mut self, specs_world: &specs::World, buf: &mut Vec<(u64, UpdateKind<P>)>) {
        self.modified.clear();
        self.inserted.clear();
        self.removed.clear();

        for event in specs_world.write_storage::<C>()
            .channel()
            .read(&mut self.reader_id)
        {
            match event {
                specs::storage::ComponentEvent::Inserted(id) => self.inserted.add(*id),
                specs::storage::ComponentEvent::Modified(id) => self.modified.add(*id),
                specs::storage::ComponentEvent::Removed(id) => self.removed.add(*id),
            };
        }

        // Generate inserted updates
        for (uid, comp, _) in (
            &specs_world.read_storage::<uid::Uid>(),
            &specs_world.read_storage::<C>(),
            &self.inserted,
        ).join() {
            buf.push(((*uid).into(), UpdateKind::Inserted(P::from(comp.clone()))));
        }

        // Generate modified updates
        for (uid, comp, _) in (
            &specs_world.read_storage::<uid::Uid>(),
            &specs_world.read_storage::<C>(),
            &self.modified,
        ).join() {
            buf.push(((*uid).into(), UpdateKind::Modified(P::from(comp.clone()))));
        }

        // Generate removed updates
        for (uid, _) in (
            &specs_world.read_storage::<uid::Uid>(),
            &self.removed,
        ).join() {
            buf.push(((*uid).into(), UpdateKind::Removed(P::Phantom::from(PhantomData::<C>))));
        }
    }
}
