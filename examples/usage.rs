use std::marker::PhantomData;
use sphynx;
use specs::{
    self,
    Builder,
};
use serde_derive::{Serialize, Deserialize};

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct Pos([f32; 3]);
impl specs::Component for Pos {
    type Storage = specs::FlaggedStorage<Self, specs::VecStorage<Self>>;
}

#[derive(Copy, Clone, Debug, Serialize, Deserialize)]
struct Color([f32; 4]);
impl specs::Component for Color {
    type Storage = specs::FlaggedStorage<Self, specs::VecStorage<Self>>;
}

// Automatically derive From<T> for Packet for each variant Packet::T(T)
sphynx::sum_type! {
    #[derive(Clone, Debug, Serialize, Deserialize)]
    enum Packet {
        Pos(Pos),
        Color(Color),
    }
}
// Automatically derive From<T> for Phantom for each variant Phantom::T(PhantomData<T>)
sphynx::sum_type! {
    #[derive(Clone, Debug, Serialize, Deserialize)]
    enum Phantom {
        Pos(PhantomData<Pos>),
        Color(PhantomData<Color>),
    }
}
impl sphynx::Packet for Packet {
    type Phantom = Phantom;
}

fn main() {
    let mut world = sphynx::World::<Packet>::new(specs::World::new(), |world| {
        world.register_synced::<Pos>();
        world.register_synced::<Color>();
    });

    let test_entity = world.create_entity_synced()
        .with(Pos([0.0; 3]))
        .with(Color([0.0; 4]))
        .build();

    println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    world.internal_mut()
        .write_storage::<Color>()
        .remove(test_entity)
        .unwrap();

    println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    let state_package = world.gen_state_package();
    println!("World 1, State Package:\n{:?}", state_package);
    let mut world2 = sphynx::World::<Packet>::from_state_package(specs::World::new(), |world| {
        world.register_synced::<Pos>();
        world.register_synced::<Color>();
    }, state_package);

    println!("World 2, State Package:\n{:?}", world2.gen_state_package());

    println!("World 2, Sync Package:\n{:?}", world2.next_sync_package());

    //world.delete_entity_synced(test_entity).unwrap();
    world.internal_mut()
        .write_storage::<Pos>()
        .remove(test_entity)
        .unwrap();

    //println!("World 1, Sync Package:\n{:?}", world.next_sync_package());

    let sync_p = world.next_sync_package();
    println!("World 1, Sync Package:\n{:?}", sync_p);
    world2.sync_with_package(sync_p);

    println!("World 2, State Package:\n{:?}", world2.gen_state_package());
}
